.PHONY: pyproject.toml
default: help

# Use docstrings after your targets to have them show up in help.
# docstrings start with #?

#REQS=requirements.txt
GAWK=gawk

help:
#? mysterious.
	@echo "Makefile for the struid package v`cat VERSION`."
	@echo "\nUsage: make [target]"
	
	@ #error checks:
	@( which ${GAWK} >/dev/null || { echo "\nError: ${GAWK} is needed to display help properly. Please install it.\n" && exit 1; } )
#	@( test -f "${REQS}" || { echo -e "\nError: You need a requirements file '${REQS}'. Please create one.\n" && exit 1; } )
	@ #show help:
	@echo "\nThe default target is '`awk '/^(default|all): /{print $$2}' Makefile | head -n1`'"
	@gawk 'match($$0,/^([a-z\-]+):/,n) {CMD=n[1];CANHAS=""}; match($$0,/^#\?\s*(.*)$$/,n) { if (CMD) { if (CANHAS) print "  "n[1]; else print "\nmake "CMD"\n  "n[1] ; CANHAS="1" } }  ' Makefile
	@echo

test:
#? Run the test suite
	@pytest

build-reqs:
#? Install requirements for building
	@echo "Installing build dependencies..."
	@python3 -m pip install --upgrade build

build: pyproject.toml build-reqs test
#? build the package
	@echo "Building package..."
	@python3 -m build

upload-reqs:
#? Install requirements for uploading
	@echo "Installing twine..."
	@python3 -m pip install --upgrade twine

upload-test: build upload-reqs
#? upload to test pypi repo
	@echo "Uploading package to TEST pip repo..."
	python3 -m twine upload --repository testpypi dist/*

release: clean new-build-version build upload-reqs
#? increment version, build, and upload to real-reals pypi
	@echo "Uploading package..."
	python3 -m twine upload dist/*

new-build-version:
#? Increment the build number in the VERSION file
	@bash -c 'ver=$$(cat VERSION); echo "Current version is $$ver";IFS="." read -ra VER <<< "$$ver" ;major="$${VER[0]}";minor="$${VER[1]}";build="$${VER[2]}";build=$$((build+1));newver="$$major.$$minor.$$build";echo "New version: $$newver";echo $$newver > VERSION'

pyproject.toml:
#? Build a new pyproject.toml, updating from VERSION
	@cp pyproject.toml pyproject.toml.bak
	@gawk -v newver="$$(cat VERSION)" -v gendate="$$(date)" 'match($$0,/^version\s*=/,ver) {nv=ver[0]; }{ if (nv) { print "version = \042" newver "\042 # Generated " gendate;nv="" } else { print } }' pyproject.toml > /tmp/pyproject.toml.new
	@mv /tmp/pyproject.toml.new pyproject.toml


clean:
#? cleanup cache/build files
	@rm -Rvf dist __pycache__ *.pyc

