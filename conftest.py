import sys, os

struid_dir = os.path.join(os.path.dirname(__file__),'src','struid')
if not os.path.exists(struid_dir):
	raise RuntimeError(f"'{struid_dir}' not found!")

sys.path.append(struid_dir)

print(f"Sys path: {sys.path}")
