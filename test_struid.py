# test_struid.py

from pytest import raises, mark

from uuid import UUID as UUID_orig, uuid4 as uuid4_orig

import struid
from struid import Struid, UUID, uuid4, testit

# our standard test string uuid value
d00fd00f = 'deadbeef-d00f-d00f-d00f-c0ffeedecade'
d00fd00f_int = int(UUID_orig(d00fd00f)) #295990755078525382164994183696159263454

trinary="+-="
d00fd00f_trinary = "=+++++-=--=++--++=+--+-=+=---+=-+-=-==+-=-======-+=--+-+=--++--+--=-=+=+=++-=-==-"


"""
The shortstr depends (obviously) on the chosen digits. Here we support a couple of common options
"""
if struid.SHORTSTR_DIGITS.startswith('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'):
	# short code produced by alphanumberic + runes + symbols
	d00fd00f_short = 'ᛓ🐼🖻💝🍆👧🍼💍ᛩ😓🍈🗑🔿' 
else:
	# just runes + symbols
	d00fd00f_short = '👰🍌🚪🙷🍥🔯💖💈📡🕛🚚🏷😤' # '🌨🚩💵👤🚡ᚮ🕓💣🐙😝🕴🕤ᛦ'
	

def test_struid_instantiates_like_a_guid():
	# given a string that is a valid hexadecimal representation of a uuid
	# 1. when we instantiate a struid with that value
	v = Struid(d00fd00f)
	print(v)
	
	# then the result is a struid
	assert type(v) is Struid
	# and it's also a real UUID, because struid inherits from that
	assert isinstance(v,UUID_orig)
	
	# and it has the correct int value
	assert v.int == d00fd00f_int
	
	# 2. when we instantiate a struid with the int value
	v2 = Struid(d00fd00f_int)
	# then we get a struid
	assert type(v2) is Struid
	# and it has the correct value
	assert v.int == v2.int
	
def test_uuid_replaced_by_struid():
	#given we imported UUID from the struid package
	
	# when we instantiate a UUID
	v = UUID(d00fd00f)
	# then it is a struid
	assert isinstance(v,Struid)
	# and it is also a UUID (from the struid package, alias class)
	assert isinstance(v,UUID)
	# and it is also a real UUID from the uuid package (because inheritance)
	assert isinstance(v,UUID_orig)
	
def test_uuid4_replaced_by_struid():
	# given we've imported uuid4 from the struid package
	# when we create a new uuid4
	v = uuid4()
	# then we get a struid
	assert isinstance(v,Struid)

def test_struid_instantiates_from_shortstr():
	# given we have a short string for a known value
	# when we instat=ntiate a struid using this value
	v = Struid(d00fd00f_short)
	# then we get a struid
	assert type(v) is Struid
	# and the valur is as expected
	assert str(v) == d00fd00f
	assert v.int == d00fd00f_int

def test_struid_instantiates_from_uuid():
	# given we have a uuid
	a_uuid = UUID_orig(d00fd00f)
	# when we try to instantiate a struid using that uuid
	v = Struid(a_uuid)
	# then it doesn't shit the bed
	# and we get back a struid
	assert type(v) is Struid
	# and it's also a UUID
	assert isinstance(v,UUID_orig)
	# and the value matches
	assert v.int == a_uuid.int
	# and they can be directly compared and are equal
	assert v == a_uuid


def test_struid_can_be_compared_with_string():
	# given we have a struid with a known value
	v = Struid(d00fd00f)
	# when we compare the uuid with the string value
	result = (v == str(d00fd00f))
	# then they are equal
	assert result is True
	
	# 2. given we compare the struid with some other string
	result = (v == "some other string")
	# then they are not equal
	assert result is not True
	
	# 3. given we compare the struid with another valid uuid string:
	result = (v == 'f00df00d-f00d-f00d-f00f-c0ffeedecade')
	# then they are not equal
	assert result is not True
	
def test_struid_is_case_insensitive():
	# given we have a known struid from a lowercase hex value
	v1 = Struid(d00fd00f.lower())
	
	# when we get a struid for the uppercase value
	v2 = Struid(d00fd00f.upper())
	
	# then they are the same
	assert v1 == v2
	
	# and they are equal with the lowercase string:
	assert v2 == d00fd00f.lower()
	
	# and they are equal to the uppercase string:
	assert v1 == d00fd00f.upper()
	
	# and the short string is also case insensitive
	short = v2.shortstr()
	assert Struid(short.lower()) == Struid(short.upper())
	
	
def test_struid_can_be_compared_with_int():
	# given we have a known struid value
	v = Struid(d00fd00f)
	# when we compare the uuid with its integer value
	assert v == int(d00fd00f_int) # then they are equal
	
	# 2. when we compare it to other integer values
	assert v != 42 # then they are not equal
	assert v != 0 
	assert v != 1
	
def test_struid_generates_shortstr():
	# given we have a known struid value
	v = Struid(d00fd00f)
	# when we get its shortstring:
	short = v.shortstr()
	# then it is a string
	assert type(short) is str
	# and it's the expected valud
	assert short == d00fd00f_short
	# and we can instantiate a new struid using that value
	v2 = Struid(short)
	# and the new struid has the same int value
	assert v2.int == v.int
	

def test_struid_shortstr_encode_decode_randomly():
	iterations = 10000
	for i in range(0,iterations):
		# given we have a random uuid
		v = uuid4()
		# when we get its short string
		short = v.shortstr()
		# then it is a string
		print(f"{i}: {v} -> {short}")
		assert type(short) is str
		
		# when we instantiate a new struid using that shortstr
		v2 = Struid(short)
		# then the new struid has the correct int value
		assert v2.int == v.int
		
def test_struid_shortstr_arbitrary_base_algo_gives_correct_hexadecimal():
	# given the struid module has its shortstr digits limited to the hexadecimal chars
	orig = struid.SHORTSTR_DIGITS
	struid.SHORTSTR_DIGITS = "0123456789abcdef"
	# given we have a d00fd00f struid
	v = Struid(d00fd00f)
	# when we get the shortstr
	short = v.shortstr()
	print("Got 'short' String: ",short)
	# then it is d00fd00f (without the hyphens) (because we picked the hex charset and the algo is the same)
	assert short == 'deadbeefd00fd00fd00fc0ffeedecade' == d00fd00f.replace('-','')
	
	# reset back to original
	struid.SHORTSTR_DIGITS = orig
	
def test_struid_run_builtin_test():
	# when we run the builtin
	# then it doesn't fail/error
	testit()

def test_get_set_digits():
	# when we call get_digits
	orig_digits = struid.get_digits()
	# then it returns the shortstr digits
	assert orig_digits == struid.SHORTSTR_DIGITS
	# 2. when we set the digits
	struid.set_digits(trinary)
	# then the shortstr_digits are set
	assert struid.SHORTSTR_DIGITS == trinary

	# and it works as you'd expect:
	trid00f = UUID(d00fd00f).shortstr()
	print(f"Trinary: {trid00f}")
	assert trid00f == d00fd00f_trinary
	
	# 3. when we return the setting to its original value
	struid.set_digits(orig_digits)
	# then the digits are set
	assert struid.SHORTSTR_DIGITS == orig_digits

same_char_err = "ValueError: You cannot include the same character more than once in new_digits"
case_err = "ValueError: You cannot include both uppercase and lowercase of the same character in new_digits"

@mark.parametrize('digits,error_msg',[
	(struid.default_shortstr_digits(),None), # you'd hope the default characters are OK...
	('010',same_char_err),
	("Aah",case_err),
	("01",None),			# OK
	(".-",None),			# OK
	("+-=",None),			# OK
	("+-=-",same_char_err),	
	("0123456789",None),
	("01234567890",same_char_err), 	# 0 repeats
	("Foo",same_char_err),
	("Fo0f",case_err),
	("0123456789ABCDEF",None), # hex!
	("0123456789abcdef",None),
	("fedcba9876543210",None), # reverse-hex!
	("0123456789abcdefg",None), # base 17
	("0123456789abcdefghijklmnopqrstuvwxyz",None), # base 27
])
def test_set_digits_disallows_repeated_chars_case_insensitive(digits,error_msg):
	"""
	if the digits are ok, pass in None as the error_msg
	"""
	
	orig_digits = struid.get_digits()
	
	if error_msg is None:
		# when I try to set the digits to a string that doesn't include the same
		#  character (case-insensitive) more than once
		struid.set_digits(digits)
		# then it doesn't throw an exception
		# and it sets the digits correctly
		assert struid.SHORTSTR_DIGITS == digits
	else:
		
		# when I try to set the digits to a string that includes the same character twice
		# then it spits the dummy	
		with raises(ValueError) as ex:
			struid.set_digits(digits) # 0 repeats
			
		assert ex.exconly() == error_msg
	
	# restore original digits
	struid.set_digits(orig_digits)
	
	#assert False

def test_default_digits_must_never_change():
	"""
	Now that struid is in the wild, we must never change the default digits
	 - doing so would break people's code
	"""
	assert struid.default_shortstr_digits() == '🌀🌁🌂🌃🌄🌅🌆🌇🌈🌉🌊🌋🌌🌍🌎🌏🌐🌑🌒🌓🌔🌕🌖🌗🌘🌙🌚🌛🌜🌝🌞🌟🌠🌡🌢🌣🌤🌥🌦🌧🌨🌩🌪🌫🌬🌭🌮🌯🌰🌱🌲🌳🌴🌵🌶🌷🌸🌹🌺🌻🌼🌽🌾🌿🍀🍁🍂🍃🍄🍅🍆🍇🍈🍉🍊🍋🍌🍍🍎🍏🍐🍑🍒🍓🍔🍕🍖🍗🍘🍙🍚🍛🍜🍝🍞🍟🍠🍡🍢🍣🍤🍥🍦🍧🍨🍩🍪🍫🍬🍭🍮🍯🍰🍱🍲🍳🍴🍵🍶🍷🍸🍹🍺🍻🍼🍽🍾🍿🎀🎁🎂🎃🎄🎅🎆🎇🎈🎉🎊🎋🎌🎍🎎🎏🎐🎑🎒🎓🎔🎕🎖🎗🎘🎙🎚🎛🎜🎝🎞🎟🎠🎡🎢🎣🎤🎥🎦🎧🎨🎩🎪🎫🎬🎭🎮🎯🎰🎱🎲🎳🎴🎵🎶🎷🎸🎹🎺🎻🎼🎽🎾🎿🏀🏁🏂🏃🏄🏅🏆🏇🏈🏉🏊🏋🏌🏍🏎🏏🏐🏑🏒🏓🏔🏕🏖🏗🏘🏙🏚🏛🏜🏝🏞🏟🏠🏡🏢🏣🏤🏥🏦🏧🏨🏩🏪🏫🏬🏭🏮🏯🏰🏱🏲🏳🏴🏵🏶🏷🏸🏹🏺🏻🏼🏽🏾🏿🐀🐁🐂🐃🐄🐅🐆🐇🐈🐉🐊🐋🐌🐍🐎🐏🐐🐑🐒🐓🐔🐕🐖🐗🐘🐙🐚🐛🐜🐝🐞🐟🐠🐡🐢🐣🐤🐥🐦🐧🐨🐩🐪🐫🐬🐭🐮🐯🐰🐱🐲🐳🐴🐵🐶🐷🐸🐹🐺🐻🐼🐽🐾🐿👀👁👂👃👄👅👆👇👈👉👊👋👌👍👎👏👐👑👒👓👔👕👖👗👘👙👚👛👜👝👞👟👠👡👢👣👤👥👦👧👨👩👪👫👬👭👮👯👰👱👲👳👴👵👶👷👸👹👺👻👼👽👾👿💀💁💂💃💄💅💆💇💈💉💊💋💌💍💎💏💐💑💒💓💔💕💖💗💘💙💚💛💜💝💞💟💠💡💢💣💤💥💦💧💨💩💪💫💬💭💮💯💰💱💲💳💴💵💶💷💸💹💺💻💼💽💾💿📀📁📂📃📄📅📆📇📈📉📊📋📌📍📎📏📐📑📒📓📔📕📖📗📘📙📚📛📜📝📞📟📠📡📢📣📤📥📦📧📨📩📪📫📬📭📮📯📰📱📲📳📴📵📶📷📸📹📺📻📼📽📾📿🔀🔁🔂🔃🔄🔅🔆🔇🔈🔉🔊🔋🔌🔍🔎🔏🔐🔑🔒🔓🔔🔕🔖🔗🔘🔙🔚🔛🔜🔝🔞🔟🔠🔡🔢🔣🔤🔥🔦🔧🔨🔩🔪🔫🔬🔭🔮🔯🔰🔱🔲🔳🔴🔵🔶🔷🔸🔹🔺🔻🔼🔽🔾🔿🕀🕁🕂🕃🕄🕅🕆🕇🕈🕉🕊🕋🕌🕍🕎🕏🕐🕑🕒🕓🕔🕕🕖🕗🕘🕙🕚🕛🕜🕝🕞🕟🕠🕡🕢🕣🕤🕥🕦🕧🕨🕩🕪🕫🕬🕭🕮🕯🕰🕱🕲🕳🕴🕵🕶🕷🕸🕹🕺🕻🕼🕽🕾🕿🖀🖁🖂🖃🖄🖅🖆🖇🖈🖉🖊🖋🖌🖍🖎🖏🖐🖑🖒🖓🖔🖕🖖🖗🖘🖙🖚🖛🖜🖝🖞🖟🖠🖡🖢🖣🖤🖥🖦🖧🖨🖩🖪🖫🖬🖭🖮🖯🖰🖱🖲🖳🖴🖵🖶🖷🖸🖹🖺🖻🖼🖽🖾🖿🗀🗁🗂🗃🗄🗅🗆🗇🗈🗉🗊🗋🗌🗍🗎🗏🗐🗑🗒🗓🗔🗕🗖🗗🗘🗙🗚🗛🗜🗝🗞🗟🗠🗡🗢🗣🗤🗥🗦🗧🗨🗩🗪🗫🗬🗭🗮🗯🗰🗱🗲🗳🗴🗵🗶🗷🗸🗹🗺🗻🗼🗽🗾🗿😀😁😂😃😄😅😆😇😈😉😊😋😌😍😎😏😐😑😒😓😔😕😖😗😘😙😚😛😜😝😞😟😠😡😢😣😤😥😦😧😨😩😪😫😬😭😮😯😰😱😲😳😴😵😶😷😸😹😺😻😼😽😾😿🙀🙁🙂🙃🙄🙅🙆🙇🙈🙉🙊🙋🙌🙍🙎🙏🙐🙑🙒🙓🙔🙕🙖🙗🙘🙙🙚🙛🙜🙝🙞🙟🙠🙡🙢🙣🙤🙥🙦🙧🙨🙩🙪🙫🙬🙭🙮🙯🙰🙱🙲🙳🙴🙵🙶🙷🙸🙹🙺🙻🙼🙽🙾🙿🚀🚁🚂🚃🚄🚅🚆🚇🚈🚉🚊🚋🚌🚍🚎🚏🚐🚑🚒🚓🚔🚕🚖🚗🚘🚙🚚🚛🚜🚝🚞🚟🚠🚡🚢🚣🚤🚥🚦🚧🚨🚩🚪🚫🚬🚭🚮🚯🚰🚱🚲🚳🚴🚵🚶🚷🚸🚹🚺🚻🚼🚽🚾🚿🛀🛁🛂🛃🛄🛅🛆🛇🛈🛉🛊🛋🛌🛍🛎🛏🛐🛑🛒🛓🛔🛕'

